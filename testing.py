import sounddevice as sd
import soundfile as sf
import numpy as np
import math
import matplotlib.pyplot as plt
from stft import analysis, synthesis

def peak_finder(x):
    peaks = []
    profile = np.array([-2, -1, 1, 2])
    minimum = 5
    for i in range(2, len(x) - 2):
        if x[i] > max(x[profile + i]) and x[i] > minimum:
            peaks.append(i)
    return peaks

def plot_it(i, j):
    power = np.abs(np.fft.rfft(broken[i,:]))**2
    plt.plot(np.arange(j) * rate / window.size,
            power[:j])
    plt.show()

def collect_peaks(broken_power):
    peaks = set()
    for i in broken_power:
        peak_ind = peak_finder(i)
        for j in peak_ind:
            peaks.add(j)
    return peaks

def round2multiple(x, difference, multiple):
    return np.round((x - difference) / multiple) * multiple + difference

def phase_decode(z1, z2, rate, time, target):
    difference = np.angle(z2 / z1) * rate / time 
    multiple = 2 * np.pi * rate / time
    return round2multiple(target, difference, multiple)

def wiggly_time(length, mu = 1, sigma = 0.01):
    return np.cumsum(sigma * np.random.randn(length) + mu)

data, rate = sf.read('pianoa4.aiff')
data = np.sum(data, axis = 1)
data = data * 0.5 / max(data)
window_size = 2*math.ceil(rate/30) # number of samples to get up 30 hz and above
window = np.sin(np.pi*np.arange(window_size) / window_size)
hop = window_size // 2
#window = np.ones(window_size)
#hop = window_size

broken = analysis(data, window, hop)
broken_rfft = np.fft.rfft(broken)
broken_power = np.abs(broken_rfft)**2
peaks = list(collect_peaks(broken_power))
peak_powers = np.apply_along_axis(max, 0, broken_power[:,peaks])
top4 = sorted(zip(peak_powers, peaks))[-4:]
peaks = list(zip(*top4))[1]

freq1 = (np.mean(phase_decode(broken_rfft[:-1, 29], broken_rfft[1:, 29],
    rate, window.size, 29 * rate / window.size)) + np.mean(
            phase_decode(broken_rfft[:-1,30], broken_rfft[1:,30], rate,
                window.size, 30 * rate / window.size))) / 2

freq2 = (np.mean(phase_decode(broken_rfft[:-1, 59], broken_rfft[1:, 59],
    rate, window.size, 59 * rate / window.size)) + np.mean(
            phase_decode(broken_rfft[:-1, 58], broken_rfft[1:,58], rate,
                window.size, 58 * rate / window.size))) / 2

power1 = broken_power[:, 29] + broken_power[:, 30]
power2 = broken_power[:, 59] + broken_power[:, 58]

start = np.argmax(power1)
x = np.arange(start, power1.size)
A = np.vstack([x, np.ones(x.size)]).T
k1, c1 = np.linalg.lstsq(A, np.log(power1[start:]))[0]

start = np.argmax(power2)
x = np.arange(start, power1.size)
A = np.vstack([x, np.ones(x.size)]).T
k2, c2 = np.linalg.lstsq(A, np.log(power2[start:]))[0]

t = np.arange(data.size // 4)
#z = np.exp(c1) * np.exp(k1*t / hop) * np.sin(2*np.pi*freq1*t/rate) + np.exp(c2) * np.exp(k2*t / hop) * np.sin(2*np.pi*freq2*t/rate)
z = c1 * np.exp(k1*t / hop) * np.sin(2*np.pi*freq1*t/rate) + c2 * np.exp(k2*t / hop) * np.sin(2*np.pi*freq2*t/rate)
z /= 100
z *= 1 - np.exp(-t / 10)
sf.write('testing9.aiff', z, rate)

#power1 * np.sin(2 * np.pi * freq1 * np.arange(data.size) / rate)

#phase_decode(broken_rfft[:-1, 29], broken_rfft[1:, 29]

new_broken_rfft = np.zeros(broken_rfft.shape, dtype = np.complex)
new_broken_rfft[:,peaks] = broken_rfft[:,peaks]
newdata = synthesis(np.fft.irfft(new_broken_rfft), window, hop)
sf.write('testing123.aiff', newdata, rate)

