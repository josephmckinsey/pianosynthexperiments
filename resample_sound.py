import sounddevice as sd
import soundfile as sf
import numpy as np
import math
import matplotlib.pyplot as plt
from stft import analysis, synthesis

data, rate = sf.read('pianoa4.aiff')
data = np.sum(data, axis = 1)
data = data * 0.5 / max(data)
window_size = 3*math.ceil(rate/30) # number of samples to get up 30 hz and above
window = np.sin(np.pi*np.arange(window_size) / window_size)
hopin = window_size // 2
hopout = window.size // 4

recon = synthesis(analysis(data, window, hopin), window, hopout)
sf.write('fast.aiff', recon, rate)
sf.write('normal.aiff', data, rate)

#data1, data2 = windowing(data, window_size)
#newdata = unwindowing(*transform((data1, data2), lambda x: change_frequency(x, np.pi/2)))
#sf.write('test.aiff', newdata, rate)
