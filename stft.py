import numpy as np

def iscola(window, hop):
    base_window = np.zeros(window.size)
    for i in range(0, window.size, hop):
        base_window[i:] += window[:(window.size - i)]

    for i in range(hop, window.size, hop):
        base_window[:(window.size - i)] += window[i:]

    return max(abs(base_window - np.mean(base_window))) > 0.001

def analysis(data, window, hop): # window must be of even size
    if (data.size < window.size):
        return

    num_windows = 1 + (data.size - window.size) // hop
    broken = np.zeros((num_windows, window.size))
    for i in range(num_windows):
        broken[i,:] = data[hop*i:(hop*i + window.size)]*window

    return broken

def synthesis(data, window, hop):
    repaired = np.zeros(hop*(data.shape[0] - 1) + window.size)
    for i in range(data.shape[0]):
        repaired[i*hop:(i*hop + window.size)] += data[i,:] * window
    
    return repaired

def change_frequency(data, factor):  # way too slow
    N = data.size
    frequencies = np.fft.fft(data)
    scaling = np.exp(2j*np.pi*factor*np.outer(np.arange(N), np.arange(N)) / N) / N
    return scaling @ frequencies

def power(section):
    return np.abs(np.fft.rfft(section))**2
